import os

train_data = []

root_path = "LibriSpeech/dev-other"
for fl in os.listdir(root_path):
		for sl in os.listdir(f"{root_path}/{fl}"):
			audio_dir_path = f"{root_path}/{fl}/{sl}"
			
			# find txt file
			decryption_file = ""
			for tl in os.listdir(audio_dir_path):
				if tl.split(".")[-1] == "txt":
					decryption_file = tl
					break

			if decryption_file == "":
				continue

			decryption_file_content = open(f"{audio_dir_path}/{decryption_file}").read()
			decryption_file_lines = decryption_file_content.split("\n")
			
			for line in decryption_file_lines:
				words = line.split(" ")
				filename = words[0]
				phrase = " ".join(words[1:])

				if filename == "":
					continue

				train_data.append((f"{audio_dir_path}/{filename}.flac", phrase))

print(train_data)